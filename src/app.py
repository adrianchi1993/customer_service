from flask import Flask,request,Response
from flask_pymongo import PyMongo
from bson.json_util import dumps,loads
from bson.objectid import ObjectId

app = Flask (__name__)
app.config['MONGO_URI'] = 'mongodb://mongo:27017/customer_service'
mongo = PyMongo(app)

@app.route('/customers', methods =['POST'])
def create_customer():
    try:
        nombre = request.json['nombre']
        apellido = request.json['apellido']
        edad = request.json['edad']
        if(nombre and nombre != ' ') and (apellido and apellido != ' ') and (edad and edad != ' '):
            myquery = {'nombre':nombre, 'apellido':apellido, 'edad':edad}
            id = mongo.db.customer.insert_one(myquery)
            response = {
                'message':'El registro fue ingresado con éxito' + str(id)
            }
        else:
            response = {
                'message':'Favor llenar todos los campos'
            }
    except:
        response = {
            'message':'No se pudo guardar el registro, Favor verificar los datos'
        }
    return response

@app.route('/customers', methods=['GET'])
def get_customers():
    try:
        customers = mongo.db.customer.find()
        customers_raw = dumps(list(customers), indent=2)
        if customers_raw == '[]':
            response = {
                'message': 'No se encontraron registros'
            }         
        else:
            response = Response(customers_raw, mimetype='application/json')
            
    except:
        response = {
            'message':'Ocurrió un error, Favor intentar más tarde'
        }
    return response

@app.route('/customers/<id>', methods=['GET'])
def get_customer(id):
    try:
        myquery = {'_id':ObjectId(id)}
        customer = mongo.db.customer.find_one(myquery)
        customer_raw = dumps(customer, indent=2)
        if customer_raw == 'null':
            response = {
                'message':'No se encontró el registro o no existe, Favor verificar los datos'
            }
        else:
            response = Response(customer_raw, mimetype='application/json')
    except:
        response = {
            'message':'Ocurrió un error, Favor intentar más tarde'
        }
    return response

@app.route('/customers/<id>', methods = ['DELETE'])
def delete_customer(id):
    try:
        myquery={'_id':ObjectId(id)}
        customer = mongo.db.customer.find_one(myquery)
        customer_raw = dumps(customer, indent=2)
        if customer_raw == 'null':
            response = {
                'message':'El registro a eliminar no se encuentra o no existe, Favor verifique los datos'
            }
        else:
            mongo.db.customer.delete_one(myquery)
            response = {
                'message':'El registro fué eliminado con éxito'
            }
    except:
        response = {
            'message':'Ocurrió un error, Favor intentar más tarde'
        }
    return response

@app.route('/customers/<id>', methods=['PUT'])
def update_customer(id):
    try:
        nombre = request.json['nombre']
        apellido = request.json['apellido']
        edad = request.json['edad']
        if (nombre and nombre != ' ') and (apellido and apellido != ' ') and (edad and edad != ' '):
            myquery = {'_id':ObjectId(id)}
            values = {'$set':{'nombre':nombre, 'apellido':apellido, 'edad':edad}}
            customer = mongo.db.customer.find_one(myquery)
            customer_raw = dumps(customer, indent=2)
            if customer_raw == 'null':
                response = {
                    'message':'El registro a modificar no se encuentra o no existe, Favor verifique los datos'
                }
            else:
                mongo.db.customer.update_one(myquery,values)
                response = {
                    'message':'El registro ' + id +' se ha actualizado con éxito'
                }      
        else:
            response = {
                'message':'Favor llenar todos los campos'
            }
    except:
        response = {
            'message':'Ocurrió un error, Favor intentar más tarde'
        }
    return response

@app.route('/prueba')
def prueba():
    return "Hola"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port =5000, debug = True)
