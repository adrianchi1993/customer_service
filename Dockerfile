FROM python:3-alpine
RUN apk add --no-cache python3-dev py3-pip
WORKDIR /app
EXPOSE 5000

COPY . /app

RUN pip --no-cache-dir install -r requirements.txt

CMD ["python3","src/app.py"]